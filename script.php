<?
ob_start();

$cont = ob_get_clean();

if (empty($_POST) && !isset($_GET["openstat"]) && !isset($_GET["utm_source"]) && !isset($_GET["yclid"]) && !isset($_GET["adid"])) {

	$_SERVER["REQUEST_URI"] = str_replace(array('?clear_cache=y','&clear_cache=y'),'',$_SERVER["REQUEST_URI"]);
	
	$name_file = $_SERVER["HTTP_HOST"]."/".str_replace(array('/',':','?'),'_',$_SERVER["REQUEST_URI"]);
	if (!is_dir("/home/seminar4/public_html/aktiv/cache/".$_SERVER["HTTP_HOST"]."/")) {
		mkdir("/home/seminar4/public_html/aktiv/cache/".$_SERVER["HTTP_HOST"]."/", 0755);
	}
	
	$file = "/home/seminar4/public_html/aktiv/cache/".$name_file;
	
	if (isset($_GET["clear_cache"]) && $_GET["clear_cache"] == 'y') {
		unlink($file);
	}
	
	if (file_exists($file) && (filemtime($file) + 3600*24) > time()) {		
		$content = file_get_contents($file);
		echo $content;
	} else {
		file_put_contents($file, $cont);
		echo $cont;
	}
} else {
	echo $cont;
}
?>